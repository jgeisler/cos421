#include <unistd.h>
#include <iostream>
#include <random>

using namespace std;

int main(int argc, char **argv) {
    uniform_int_distribution<unsigned> numbers{33, 126};
    random_device rd;
    default_random_engine engine{rd()};

    for (int i = 0; i < 1024; ++i) {
	unsigned value = numbers(engine);
	write(1, &value, 1);
    }
    cout << "\n";

    return 0;
}
