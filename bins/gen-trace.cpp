#include <iostream>
#include <getopt.h>
#include <cstdlib>

using namespace std;

const int PAGE_SIZE = 4096;

void print_page(ostream& out, int page) {
    int offset = lrand48() % PAGE_SIZE;
    out << "I " << hex << (page * PAGE_SIZE + offset) << dec << ", 8\n";
}

void gen_sequence(int max_pages) {
    for (int page = 0; page < max_pages; ++page) {
	print_page(cout, page);
    }
}

void gen_loop(int max_pages, int total_addresses) {
    for (int addresses = 0; addresses < total_addresses; ) {
	for (int page = 0; page < max_pages; ++page) {
	    print_page(cout, page);
	    ++addresses;
	}
    }
}

void gen_random(int max_pages, int total_addresses) {
    for (int addresses = 0; addresses < total_addresses; ++addresses) {
	print_page(cout, lrand48() % max_pages);
    }
}

int main(int argc, char **argv) {
    int do_seq = 0;
    int do_loop = 0;
    int do_random = 0;
    int max_pages = 10;
    int total_addresses = 25;

    struct option options[] = {
	{ "sequential", no_argument, &do_seq, 1 },
	{ "loop", no_argument, &do_loop, 1 },
	{ "random", no_argument, &do_random, 1},
	{ "max-pages", required_argument, nullptr, 'M'},
	{ "total-addresses", required_argument, nullptr, 't'},
	{ nullptr, 0, nullptr, 0},
    };

    for (int c = 0;
	 (c = getopt_long(argc, argv, "", options, nullptr)) != -1;
	)
    {
	switch (c) {
	  case 'M':
	    max_pages = atoi(optarg);
	    break;

	  case 't':
	    total_addresses = atoi(optarg);
	    break;
	}
    }

    srand48(time(nullptr));

    if (do_seq) {
	gen_sequence(max_pages);
    } else if (do_loop) {
	gen_loop(max_pages, total_addresses);
    } else if (do_random) {
	gen_random(max_pages, total_addresses);
    } else {
	cerr << "No legal generator chosen\n";
    }

    return 0;
}
