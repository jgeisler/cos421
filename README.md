This is the testing framework for the COS 421 labs.  Currently, it
holds the initial tests for the TU sh lab, process trace lab, and the fs lab.
The tests may be augmented as the semester progresses.
